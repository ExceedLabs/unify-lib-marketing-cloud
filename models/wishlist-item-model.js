const _ = require('lodash');
const WishlistItemService = require('../services/marketing/wishlist-item/wishlist-item-service');
const WishlistItemConverter = require('../services/marketing/wishlist-item/wishlist-item-converter');
const GenericHelper = require('../services/helpers/unify-helpers/generic-helper');
const RedisService = require('../services/helpers/unify-helpers/redis-helper');

class WishlistItemModel {
  static async isAtLeastOneWishlistItemFromArrayInDataExtension(obj, currentLib, body = []) {
    const { MessageService } = obj.privateLibraries;
    const [head, ...tail] = body;

    try {
      if (!body.length) {
        return false;
      }

      const searchResult = await WishlistItemService
        .searchWishlistItemById(currentLib.credentials, head.id)
        .catch((error) => {
          console.error('Error on searching wishlist item! Details: ');
          console.error(error);
          return ({ data: error.stack, res: { statusCode: 500 }, message: error.message });
        });

      if (!GenericHelper.isSuccess(searchResult.res.statusCode)) {
        return MessageService.getError(
          searchResult.data,
          searchResult.res.statusCode,
          searchResult.message,
        );
      }

      if (searchResult.body.Results.length > 0) {
        return MessageService.getError(searchResult.body.Results, 400, 'Item already exists in data extension.');
      }

      return this.isAtLeastOneWishlistItemFromArrayInDataExtension(obj, currentLib, tail);
    } catch (error) {
      console.error('Error to verify if is at least one wishlist item from the payload in the data extension.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async isAtLeastOneWishlistItemFromArrayNotInDataExtension(obj, currentLib, body = []) {
    const { MessageService } = obj.privateLibraries;
    const [head, ...tail] = body;

    try {
      if (!body.length) {
        return false;
      }

      const searchResult = await WishlistItemService
        .searchWishlistItemById(currentLib.credentials, head.id)
        .catch((error) => {
          console.error('Error on searching wishlist item! Details: ');
          console.error(error);
          return ({ data: error.stack, res: { statusCode: 500 }, message: error.message });
        });

      if (!GenericHelper.isSuccess(searchResult.res.statusCode)) {
        return MessageService.getError(
          searchResult.data,
          searchResult.res.statusCode,
          searchResult.message,
        );
      }

      if (searchResult.body.Results.length <= 0) {
        return MessageService.getError(head, 400, 'Item not exists in data extension.');
      }

      return this.isAtLeastOneWishlistItemFromArrayNotInDataExtension(obj, currentLib, tail);
    } catch (error) {
      console.error('Error to verify if is at least one wishlist item from the payload not in the data extension.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async searchWishlistItemById(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { params } = obj.req;

    try {
      const searchResult = await WishlistItemService
        .searchWishlistItemById(currentLib.credentials, params.wishlistItemId)
        .catch((error) => {
          console.error('Error on searching wishlist item! Details: ');
          console.error(error);
          return ({ data: error.stack, res: { statusCode: 500 }, message: error.message });
        });

      if (!GenericHelper.isSuccess(searchResult.res.statusCode)) {
        return MessageService.getError(
          searchResult.data,
          searchResult.res.statusCode,
          searchResult.message,
        );
      }

      const formattedResponse = await this.formatGetResponse(obj, searchResult.body.Results);
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, formattedResponse);
      const dataReplaced = Converters.replaceFields(data, map);
      const finalResult = (dataReplaced.length) ? dataReplaced[0] : {};

      return MessageService.getSuccess(finalResult);
    } catch (error) {
      console.error('Error to search wishlist item.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async listWishlistItems(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;

    try {
      const listResult = await WishlistItemService
        .listWishlistItems(currentLib.credentials)
        .catch((error) => {
          console.error('Error on listing wishlist items! Details: ');
          console.error(error);
          return ({ error, res: { statusCode: 500 } });
        });

      if (!GenericHelper.isSuccess(listResult.res.statusCode)) {
        return MessageService.getError(
          listResult.error,
          listResult.res.statusCode,
        );
      }

      const formattedResponse = await this.formatGetResponse(obj, listResult.body.Results);
      const finalResult = [];

      _.each(formattedResponse, (responseItem) => {
        const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [responseItem]);
        const dataReplaced = Converters.replaceFields(data, map);
        finalResult.push((dataReplaced.length) ? dataReplaced[0] : {});
      });

      return MessageService.getSuccess(finalResult);
    } catch (error) {
      console.error('Error to list wishlist items.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async createWishlistItem(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { body } = obj.req;

    try {
      const { map, data } = WishlistItemConverter.getDataMapToWishlistItem(obj, [body]);
      const dataReplaced = Converters.replaceFields(data, map);

      const searchResult = await WishlistItemService
        .searchWishlistItemById(currentLib.credentials, dataReplaced[0].id)
        .catch((error) => {
          console.error('Error on searching wishlist item! Details: ');
          console.error(error);
          return ({ data: error.stack, res: { statusCode: 500 }, message: error.message });
        });

      if (!GenericHelper.isSuccess(searchResult.res.statusCode)) {
        return MessageService.getError(
          searchResult.data,
          searchResult.res.statusCode,
          searchResult.message,
        );
      }

      if (searchResult.body.Results.length > 0) {
        return MessageService.getError(
          searchResult.body.Results,
          400,
          'Item already exists in data extension.',
        );
      }

      const newObj = {
        ...obj,
        req: {
          ...obj.req,
          body: {
            ...dataReplaced[0],
          },
        },
      };

      return this.createItem(newObj, currentLib);
    } catch (error) {
      console.error('Error to create wishlist item.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async createItem(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { body } = obj.req;
    const { assets } = currentLib.allowedObjects[0];
    const formattedBody = { values: {} };

    try {
      const credentials = await RedisService.retrieveCredentialsFromRedis(obj);

      Object.entries(body).forEach((entry) => {
        const key = entry[0];
        const value = entry[1];

        if (key !== 'id') {
          Object.assign(formattedBody.values, { [key]: value });
        }
      });

      const result = await WishlistItemService
        .upsertWishlistItem(credentials || currentLib.credentials, body.id, formattedBody, assets)
        .catch((error) => {
          console.error('Error on creating wishlist item! Details: ');
          console.error(error);
          return ({
            data: error.response.data,
            status: error.response.status,
            message: error.message,
          });
        });

      if (!GenericHelper.isSuccess(result.status)) {
        if (result.status === 401) {
          const resultToken = await WishlistItemService
            .refreshAccessToken(currentLib.credentials)
            .catch((error) => {
              console.error('Error on refreshing token! Details: ');
              console.error(error);
              return ({
                data: error.response.data,
                status: error.response.status,
                message: error.message,
              });
            });

          if (GenericHelper.isSuccess(resultToken.status)) {
            const newCurrentLib = await RedisService
              .saveCredentialsInRedis(obj, currentLib, resultToken.data);

            return this.createItem(obj, newCurrentLib);
          }

          // TODO: retry refreshing token?
          return MessageService.getError(resultToken.data, resultToken.status, resultToken.message);
        }

        return MessageService.getError(result.data, result.status, result.message);
      }

      const formattedResponse = await this.formatUpsertResponse(obj, result.data);
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [formattedResponse]);
      const dataReplaced = Converters.replaceFields(data, map);
      const finalResult = (dataReplaced.length) ? dataReplaced[0] : {};

      return MessageService.getSuccess(finalResult, result.status);
    } catch (error) {
      console.error('Error to create wishlist item.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async createWishlistItems(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { body } = obj.req;
    const replacedBody = [];

    try {
      _.each(body, (item) => {
        const { map, data } = WishlistItemConverter.getDataMapToWishlistItem(obj, [item]);
        const replacedItem = Converters.replaceFields(data, map);

        return replacedBody.push(replacedItem[0]);
      });

      const itemFoundTry = await this.isAtLeastOneWishlistItemFromArrayInDataExtension(
        obj, currentLib, replacedBody,
      );

      if (itemFoundTry) {
        return MessageService.getError(
          itemFoundTry.data,
          itemFoundTry.status,
          itemFoundTry.message,
        );
      }

      const newObj = {
        ...obj,
        req: {
          ...obj.req,
          body: replacedBody,
        },
      };

      return this.createItems(newObj, currentLib);
    } catch (error) {
      console.error('Error to create wishlist items.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async createItems(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { body } = obj.req;
    const { assets } = currentLib.allowedObjects[0];
    let formattedBody = [];

    try {
      const credentials = await RedisService.retrieveCredentialsFromRedis(obj);

      formattedBody = _.map(body, (elemObj) => {
        const newObj = { keys: {}, values: {} };

        Object.entries(elemObj).forEach((entry) => {
          const key = entry[0];
          const value = entry[1];

          if (key === 'id') {
            Object.assign(newObj.keys, { [key]: value });
          } else {
            Object.assign(newObj.values, { [key]: value });
          }
        });

        return newObj;
      });

      const result = await WishlistItemService
        .upsertWishlistItems(credentials || currentLib.credentials, formattedBody, assets)
        .catch((error) => {
          console.error('Error on creating wishlist items! Details: ');
          console.error(error);
          return ({
            data: error.response.data,
            status: error.response.status,
            message: error.message,
          });
        });

      if (!GenericHelper.isSuccess(result.status)) {
        if (result.status === 401) {
          const resultToken = await WishlistItemService
            .refreshAccessToken(currentLib.credentials)
            .catch((error) => {
              console.error('Error on refreshing token! Details: ');
              console.error(error);
              return ({
                data: error.response.data,
                status: error.response.status,
                message: error.message,
              });
            });

          if (GenericHelper.isSuccess(resultToken.status)) {
            const newCurrentLib = await RedisService
              .saveCredentialsInRedis(obj, currentLib, resultToken.data);

            return this.createItems(obj, newCurrentLib);
          }

          // TODO: retry refreshing token?
          return MessageService.getError(resultToken.data, resultToken.status, resultToken.message);
        }

        return MessageService.getError(result.data, result.status, result.message);
      }

      const formattedResponse = await this.formatUpsertResponse(obj, result.data);
      const finalResult = [];

      _.each(formattedResponse, (responseItem) => {
        const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [responseItem]);
        const dataReplaced = Converters.replaceFields(data, map);
        finalResult.push((dataReplaced.length) ? dataReplaced[0] : {});
      });

      return MessageService.getSuccess(finalResult, result.status);
    } catch (error) {
      console.error('Error to create wishlist items.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async updateWishlistItem(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { params, body } = obj.req;

    try {
      const { map, data } = WishlistItemConverter.getDataMapToWishlistItem(obj, [body]);
      const dataReplaced = Converters.replaceFields(data, map);
      const dataReplacedWithoutNull = GenericHelper.removeAttributesNull(dataReplaced);
      const payloadReplaced = dataReplacedWithoutNull[0];

      const searchResult = await WishlistItemService
        .searchWishlistItemById(currentLib.credentials, params.wishlistItemId)
        .catch((error) => {
          console.error('Error on searching wishlist item! Details: ');
          console.error(error);
          return ({ data: error.stack, res: { statusCode: 500 }, message: error.message });
        });

      if (!GenericHelper.isSuccess(searchResult.res.statusCode)) {
        return MessageService.getError(
          searchResult.data,
          searchResult.res.statusCode,
          searchResult.message,
        );
      }

      if (searchResult.body.Results.length <= 0) {
        return MessageService.getError(
          searchResult.body.Results,
          400,
          'Item not found in data extension.',
        );
      }

      const newObj = {
        ...obj,
        req: {
          ...obj.req,
          body: {
            ...payloadReplaced,
          },
        },
      };

      return this.updateItem(newObj, currentLib);
    } catch (error) {
      console.error('Error to update wishlist item.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async updateItem(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { params, body } = obj.req;
    const { assets } = currentLib.allowedObjects[0];
    const formattedBody = { values: {} };

    try {
      const credentials = await RedisService.retrieveCredentialsFromRedis(obj);

      Object.entries(body).forEach((entry) => {
        const key = entry[0];
        const value = entry[1];

        Object.assign(formattedBody.values, { [key]: value });
      });

      const result = await WishlistItemService
        .upsertWishlistItem(credentials || currentLib.credentials,
          params.wishlistItemId, formattedBody, assets)
        .catch((error) => {
          console.error('Error on updating wishlist item! Details: ');
          console.error(error);
          return ({
            data: error.response.data,
            status: error.response.status,
            message: error.message,
          });
        });

      if (!GenericHelper.isSuccess(result.status)) {
        if (result.status === 401) {
          const resultToken = await WishlistItemService
            .refreshAccessToken(currentLib.credentials)
            .catch((error) => {
              console.error('Error on refreshing token! Details: ');
              console.error(error);
              return ({
                data: error.response.data,
                status: error.response.status,
                message: error.message,
              });
            });

          if (GenericHelper.isSuccess(resultToken.status)) {
            const newCurrentLib = await RedisService
              .saveCredentialsInRedis(obj, currentLib, resultToken.data);

            return this.updateItem(obj, newCurrentLib);
          }

          // TODO: retry refreshing token?
          return MessageService.getError(resultToken.data, resultToken.status, resultToken.message);
        }

        return MessageService.getError(result.data, result.status, result.message);
      }

      const formattedResponse = await this.formatUpsertResponse(obj, result.data);
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [formattedResponse]);
      const dataReplaced = Converters.replaceFields(data, map);
      const finalResult = (dataReplaced.length) ? dataReplaced[0] : {};

      return MessageService.getSuccess(finalResult, result.status);
    } catch (error) {
      console.error('Error to update wishlist item.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async updateWishlistItems(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { body } = obj.req;
    const replacedBody = [];

    try {
      _.each(body, (item) => {
        const { map, data } = WishlistItemConverter.getDataMapToWishlistItem(obj, [item]);
        const replacedItem = Converters.replaceFields(data, map);
        const replacedItemWithoutNull = GenericHelper.removeAttributesNull(replacedItem);
        const payloadReplaced = replacedItemWithoutNull[0];

        return replacedBody.push(payloadReplaced);
      });

      const itemNotFoundTry = await this.isAtLeastOneWishlistItemFromArrayNotInDataExtension(
        obj, currentLib, replacedBody,
      );

      if (itemNotFoundTry) {
        return MessageService.getError(
          itemNotFoundTry.data,
          itemNotFoundTry.status,
          itemNotFoundTry.message,
        );
      }

      const newObj = {
        ...obj,
        req: {
          ...obj.req,
          body: replacedBody,
        },
      };

      return this.updateItems(newObj, currentLib);
    } catch (error) {
      console.error('Error to create wishlist item.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async updateItems(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { body } = obj.req;
    const { assets } = currentLib.allowedObjects[0];
    let formattedBody = [];

    try {
      const credentials = await RedisService.retrieveCredentialsFromRedis(obj);

      formattedBody = _.map(body, (elemObj) => {
        const newObj = { keys: {}, values: {} };

        Object.entries(elemObj).forEach((entry) => {
          const key = entry[0];
          const value = entry[1];

          if (key === 'id') {
            Object.assign(newObj.keys, { [key]: value });
          } else {
            Object.assign(newObj.values, { [key]: value });
          }
        });

        return newObj;
      });

      const result = await WishlistItemService
        .upsertWishlistItems(credentials || currentLib.credentials, formattedBody, assets)
        .catch((error) => {
          console.error('Error on updating wishlist items! Details: ');
          console.error(error);
          return ({
            data: error.response.data,
            status: error.response.status,
            message: error.message,
          });
        });

      if (!GenericHelper.isSuccess(result.status)) {
        if (result.status === 401) {
          const resultToken = await WishlistItemService
            .refreshAccessToken(currentLib.credentials)
            .catch((error) => {
              console.error('Error on refreshing token! Details: ');
              console.error(error);
              return ({
                data: error.response.data,
                status: error.response.status,
                message: error.message,
              });
            });

          if (GenericHelper.isSuccess(resultToken.status)) {
            const newCurrentLib = await RedisService
              .saveCredentialsInRedis(obj, currentLib, resultToken.data);

            return this.updateItems(obj, newCurrentLib);
          }

          // TODO: retry refreshing token?
          return MessageService.getError(resultToken.data, resultToken.status, resultToken.message);
        }

        return MessageService.getError(result.data, result.status, result.message);
      }

      const formattedResponse = await this.formatUpsertResponse(obj, result.data);
      const finalResult = [];

      _.each(formattedResponse, (responseItem) => {
        const { map, data } = WishlistItemConverter.getDataMapToCore(obj, [responseItem]);
        const dataReplaced = Converters.replaceFields(data, map);
        finalResult.push((dataReplaced.length) ? dataReplaced[0] : {});
      });

      return MessageService.getSuccess(finalResult, result.status);
    } catch (error) {
      console.error('Error to create wishlist item.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async deleteWishlistItem(obj, currentLib) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { params } = obj.req;

    try {
      const deleteResult = await WishlistItemService
        .deleteWishlistItem(currentLib.credentials, params.wishlistItemId)
        .catch((error) => {
          console.error('Error on deleting wishlist item! Details: ');
          console.error(error);
          return ({ data: error.stack, res: { statusCode: 500 }, message: error.message });
        });

      if (!GenericHelper.isSuccess(deleteResult.res.statusCode)) {
        return MessageService.getError(
          deleteResult.data,
          deleteResult.res.statusCode,
          deleteResult.message,
        );
      }

      const formattedResponse = await this.formatDeleteResponse(obj, deleteResult.body.Results);
      const { map, data } = WishlistItemConverter.getDataMapToCore(obj, formattedResponse);
      const dataReplaced = Converters.replaceFields(data, map);
      const finalResult = (dataReplaced.length) ? dataReplaced[0] : {};

      return MessageService.getSuccess(finalResult);
    } catch (error) {
      console.error('Error to delete wishlist item.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async formatUpsertResponse(obj, data) {
    const { MessageService } = obj.privateLibraries;

    try {
      if (Array.isArray(data)) {
        const formattedResponse = [];

        _.each(data, (item) => {
          const formattedItem = {};

          Object.entries(item).forEach((objEntry) => {
            Object.entries(objEntry[1]).forEach((entry) => {
              const key = entry[0];
              const value = entry[1];

              Object.assign(formattedItem, { [key]: value });
            });
          });

          formattedResponse.push(formattedItem);
        });

        return formattedResponse;
      }

      const formattedResponse = {};

      Object.entries(data).forEach((objEntry) => {
        Object.entries(objEntry[1]).forEach((entry) => {
          const key = entry[0];
          const value = entry[1];

          Object.assign(formattedResponse, { [key]: value });
        });
      });

      return formattedResponse;
    } catch (error) {
      console.error('Error to format UPSERT response.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async formatGetResponse(obj, data) {
    const { MessageService } = obj.privateLibraries;

    try {
      const formattedResponse = [];

      _.each(data, (item) => {
        // Property is an array of objects.
        // Each object has two entries: Name and Value corresponding every property.
        // "Property": [{ "Name": "id", "Value": "15" }, ...]
        const { Property } = item.Properties;
        const formattedItem = {};

        _.each(Property, (prop) => {
          Object.assign(formattedItem, { [prop.Name]: prop.Value });
        });

        formattedResponse.push(formattedItem);
      });

      return formattedResponse;
    } catch (error) {
      console.error('Error to format GET response.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }

  static async formatDeleteResponse(obj, data) {
    const { MessageService } = obj.privateLibraries;

    try {
      const formattedResponse = [];

      _.each(data, (item) => {
        const { Key } = item.Object.Keys;
        const formattedItem = {};

        Object.assign(formattedItem, { [Key.Name]: Key.Value });
        formattedResponse.push(formattedItem);
      });

      return formattedResponse;
    } catch (error) {
      console.error('Error to format DELETE response.');
      throw (MessageService.getError(error.stack, 500, error.message));
    }
  }
}

module.exports = WishlistItemModel;
