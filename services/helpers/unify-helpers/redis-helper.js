class RedisHelper {
  static getRedisInstance(obj) {
    return obj.privateLibraries.RedisService;
  }

  static async saveCredentialsInRedis(obj, currentLib, refreshedToken) {
    const redis = this.getRedisInstance(obj);
    const updatedCredentials = {
      ...currentLib.credentials,
      ...refreshedToken,
    };
    const updatedCurrentLib = {
      ...currentLib,
      credentials: {
        ...updatedCredentials,
      },
    };

    await redis.set(`${obj.infoOrganization.accountId}_credentials`, updatedCredentials, 'EX', 720)
      .catch((error) => {
        // TODO: handle error (retry to update?)
        console.error('Failed to save credentials in Redis DB.');
        console.error(error);
      });

    return updatedCurrentLib;
  }

  static async retrieveCredentialsFromRedis(obj) {
    try {
      const redis = this.getRedisInstance(obj);
      const result = await redis.get(`${obj.infoOrganization.accountId}_credentials`)
        .catch(error => error);

      return JSON.parse(result.data);
    } catch (error) {
      console.error('Failed to retrieve credentials from Redis DB.');
      return null;
    }
  }
}

module.exports = RedisHelper;
