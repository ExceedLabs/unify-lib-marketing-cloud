const _ = require('lodash');

class GenericHelper {
  static isSuccess(status) {
    return status >= 200 && status <= 304;
  }

  static removeAttributesNull(allData) {
    try {
      return _.map(allData, (data) => {
        let newObj = {};

        for (const key in data) {
          if (data[key]) {
            newObj = { ...newObj, ...{ [key]: data[key] } };
          }
        }

        return newObj;
      });
    } catch (error) {
      return {};
    }
  }
}

module.exports = GenericHelper;
