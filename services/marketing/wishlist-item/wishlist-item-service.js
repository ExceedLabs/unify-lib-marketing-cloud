const Axios = require('axios');
const ETClient = require('../../../client/et.client');

class WishlistItemService {
  static async refreshAccessToken(credentials) {
    const { authBaseUri, clientId, clientSecret } = credentials;
    const payload = { clientId, clientSecret };

    return Axios({
      method: 'post',
      url: `${authBaseUri}/v1/requestToken`,
      data: payload,
    });
  }

  static async searchWishlistItemById(credentials, wishlistItemId) {
    const { clientId, clientSecret } = credentials;
    const client = await ETClient.generateFuelSDKClient(clientId, clientSecret);

    const options = {
      Name: 'osf_wishlist_item',
      props: ['id', 'type', 'is_public', 'product_id', 'name', 'category_id', 'resource_state', 'description', 'priority', 'event_type', 'event_type_value', 'quantity', 'purchased_quantity', 'created_at', 'updated_at'],
      filter: {
        leftOperand: 'id',
        operator: 'equals',
        rightOperand: wishlistItemId,
      },
    };

    const deRow = client.dataExtensionRow(options);

    return new Promise(async (resolve, reject) => {
      deRow.get((err, response) => {
        if (err) {
          reject(err);
        } else {
          resolve(response);
        }
      });
    });
  }

  static async listWishlistItems(credentials) {
    const { clientId, clientSecret } = credentials;
    const client = await ETClient.generateFuelSDKClient(clientId, clientSecret);

    const options = {
      Name: 'osf_wishlist_item',
      props: ['id', 'type', 'is_public', 'product_id', 'name', 'category_id', 'resource_state', 'description', 'priority', 'event_type', 'event_type_value', 'quantity', 'purchased_quantity', 'created_at', 'updated_at'],
    };

    const deRow = client.dataExtensionRow(options);

    return new Promise(async (resolve, reject) => {
      deRow.get((err, response) => {
        if (err) {
          reject(err);
        } else {
          resolve(response);
        }
      });
    });
  }

  // Synchronous call
  static async upsertWishlistItem(credentials, wishlistItemId, payload, assets) {
    const { restBaseUri, tokenType, accessToken } = credentials;
    const { dataExtensionExternalKey } = assets;

    return Axios({
      method: 'put',
      url: `${restBaseUri}/hub/v1/dataevents/key:${dataExtensionExternalKey}/rows/id:${wishlistItemId}`,
      headers: { Authorization: `${tokenType} ${accessToken}` },
      data: payload,
    });
  }

  // Synchronous call
  static async upsertWishlistItems(credentials, payload, assets) {
    const { restBaseUri, tokenType, accessToken } = credentials;
    const { dataExtensionExternalKey } = assets;

    return Axios({
      method: 'post',
      url: `${restBaseUri}/hub/v1/dataevents/key:${dataExtensionExternalKey}/rowset`,
      headers: { Authorization: `${tokenType} ${accessToken}` },
      data: payload,
    });
  }

  static async deleteWishlistItem(credentials, wishlistItemId) {
    const { clientId, clientSecret } = credentials;
    const client = await ETClient.generateFuelSDKClient(clientId, clientSecret);

    const options = {
      Name: 'osf_wishlist_item',
      props: { id: wishlistItemId },
    };

    const deRow = client.dataExtensionRow(options);

    return new Promise(async (resolve, reject) => {
      deRow.delete((err, response) => {
        if (err) {
          reject(err);
        } else {
          resolve(response);
        }
      });
    });
  }
}

module.exports = WishlistItemService;
