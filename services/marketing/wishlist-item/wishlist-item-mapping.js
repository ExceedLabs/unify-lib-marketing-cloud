const FieldsConstant = require('../wishlist-item/wishlist-item-constant');

const FIELD_WISHLIST_ITEM = FieldsConstant.fields;


class WishlistItemMapping {
  static getWishlistItemFields(fieldsCore) {
    return {
      [FIELD_WISHLIST_ITEM.type]: fieldsCore.type,
      [FIELD_WISHLIST_ITEM.id]: fieldsCore.id,
      [FIELD_WISHLIST_ITEM.is_public]: fieldsCore.isPublic,
      [FIELD_WISHLIST_ITEM.product_id]: fieldsCore.productId,
      [FIELD_WISHLIST_ITEM.name]: fieldsCore.name,
      [FIELD_WISHLIST_ITEM.category_id]: fieldsCore.categoryId,
      [FIELD_WISHLIST_ITEM.resource_state]: fieldsCore.resourceState,
      [FIELD_WISHLIST_ITEM.description]: fieldsCore.description,
      [FIELD_WISHLIST_ITEM.priority]: fieldsCore.priority,
      [FIELD_WISHLIST_ITEM.event_type]: fieldsCore.eventType,
      [FIELD_WISHLIST_ITEM.event_type_value]: fieldsCore.eventTypeValue,
      [FIELD_WISHLIST_ITEM.quantity]: fieldsCore.quantity,
      [FIELD_WISHLIST_ITEM.purchased_quantity]: fieldsCore.purchasedQuantity,
      [FIELD_WISHLIST_ITEM.created_at]: fieldsCore.createdAt,
      [FIELD_WISHLIST_ITEM.updated_at]: fieldsCore.updatedAt,
    };
  }

  static getCoreFields(fieldsCore) {
    return {
      [fieldsCore.type]: FIELD_WISHLIST_ITEM.type,
      [fieldsCore.id]: FIELD_WISHLIST_ITEM.id,
      [fieldsCore.isPublic]: FIELD_WISHLIST_ITEM.is_public,
      [fieldsCore.productId]: FIELD_WISHLIST_ITEM.product_id,
      [fieldsCore.name]: FIELD_WISHLIST_ITEM.name,
      [fieldsCore.categoryId]: FIELD_WISHLIST_ITEM.category_id,
      [fieldsCore.resourceState]: FIELD_WISHLIST_ITEM.resource_state,
      [fieldsCore.description]: FIELD_WISHLIST_ITEM.description,
      [fieldsCore.priority]: FIELD_WISHLIST_ITEM.priority,
      [fieldsCore.eventType]: FIELD_WISHLIST_ITEM.event_type,
      [fieldsCore.eventTypeValue]: FIELD_WISHLIST_ITEM.event_type_value,
      [fieldsCore.quantity]: FIELD_WISHLIST_ITEM.quantity,
      [fieldsCore.purchasedQuantity]: FIELD_WISHLIST_ITEM.purchased_quantity,
      [fieldsCore.createdAt]: FIELD_WISHLIST_ITEM.created_at,
      [fieldsCore.updatedAt]: FIELD_WISHLIST_ITEM.updated_at,
    };
  }
}

module.exports = WishlistItemMapping;
