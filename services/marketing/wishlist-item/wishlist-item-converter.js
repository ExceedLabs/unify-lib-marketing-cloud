const WishlistItemMapping = require('../wishlist-item/wishlist-item-mapping');

class WishlistItemConverter {
  static getDataMapToCore(obj, wishlistItemData) {
    const { MessageService } = obj.privateLibraries;

    try {
      const { fieldsCore } = obj.privateLibraries.Converters;
      const { libObjId } = obj;

      const map = {
        list: libObjId,
        item: {
          ...WishlistItemMapping.getCoreFields(fieldsCore),
        },
      };

      const data = {
        [libObjId]: wishlistItemData,
      };

      return {
        map,
        data,
      };
    } catch (error) {
      return MessageService.getError(error.stack, 500, 'Error replacing wishlist item fields.');
    }
  }

  static getDataMapToWishlistItem(obj, wishlistItemData) {
    const { MessageService } = obj.privateLibraries;

    try {
      const { fieldsCore } = obj.privateLibraries.Converters;
      const { libObjId } = obj;

      const map = {
        list: libObjId,
        item: {
          ...WishlistItemMapping.getWishlistItemFields(fieldsCore),
        },
      };

      const data = {
        [libObjId]: wishlistItemData,
      };

      return {
        map,
        data,
      };
    } catch (error) {
      return MessageService.getError(error.stack, 500, 'Error replacing wishlist item fields.');
    }
  }
}

module.exports = WishlistItemConverter;
