module.exports = {
  methods: {
    get: 'GET',
    post: 'POST',
    put: 'PUT',
  },
  fields: {
    type: 'type',
    id: 'id',
    is_public: 'is_public',
    product_id: 'product_id',
    name: 'name',
    category_id: 'category_id',
    resource_state: 'resource_state',
    description: 'description',
    priority: 'priority',
    event_type: 'event_type',
    event_type_value: 'event_type_value',
    quantity: 'quantity',
    purchased_quantity: 'purchased_quantity',
    created_at: 'created_at',
    updated_at: 'updated_at',
  },
};
