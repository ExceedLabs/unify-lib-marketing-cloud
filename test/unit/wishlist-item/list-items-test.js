require('../../resources/load-env');
const Chai = require('chai');
const CoreItemStandard = require('../../resources/core/wishlist-item');
const ConverterHelper = require('../../resources/core/conversor-helper');
const GenericHelper = require('../../../services/helpers/unify-helpers/generic-helper');
const redisService = require('../../resources/core/redis-service');
const messageService = require('../../resources/core/message-status-service');
const Model = require('../../../models/wishlist-item-model');

Chai.should();

describe('## WISHLIST ITEM - LIST RESOURCE ##', () => {
  const testTimeAt = new Date().toISOString();
  const itemId = 999999999;
  const newBody = {
    id: itemId, isPublic: true, productId: 2, name: `Test at ${testTimeAt}`,
  };
  const expiredAccessToken = '16J60MT18MqJdAnNabCImunX';
  const currentLib = {
    credentials: {
      authBaseUri: 'https://mc2m2z16rt5bqpz0jxhcp50yyg70.auth.marketingcloudapis.com',
      restBaseUri: 'https://mc2m2z16rt5bqpz0jxhcp50yyg70.rest.marketingcloudapis.com',
      tokenType: 'Bearer',
      accessToken: expiredAccessToken,
      clientId: 'eq7k9f4w0x6wf51z7f0vb3bd',
      clientSecret: 'qjKSY8GwM1C6iTreBNrNlmEc',
    },
    allowedObjects: [
      {
        assets: {
          dataExtensionName: 'osf_wishlist_item',
          max: 10,
          dataExtensionExternalKey: '7E8F8D24-8835-4ED3-A2B8-7FC50C19B863',
        },
      },
    ],
  };
  const obj = {
    infoOrganization: {
      accountId: 'admin@osf-global-dev.com',
    },
    privateLibraries: {
      MessageService: messageService,
      RedisService: redisService,
      Converters: {
        fieldsCore: {
          ...CoreItemStandard,
        },
        replaceFields: ConverterHelper.replaceFields,
      },
    },
    req: {
      params: {
        wishlistItemId: '',
      },
      body: {
        ...newBody,
      },
    },
    libObjId: 'wishlistItem',
  };

  it('#01 list items', (done) => {
    Model.listWishlistItems(obj, currentLib).then((successList) => {
      GenericHelper.isSuccess(successList.status).should.equal(true);
      done();
    });
  });

  // TO DO
  // incorrect credentials
  // misspelled DE
  // incorrect DE external key
});
