require('../../resources/load-env');
const _ = require('lodash');
const Chai = require('chai');
const CoreItemStandard = require('../../resources/core/wishlist-item');
const ConverterHelper = require('../../resources/core/conversor-helper');
const GenericHelper = require('../../../services/helpers/unify-helpers/generic-helper');
const redisService = require('../../resources/core/redis-service');
const messageService = require('../../resources/core/message-status-service');
const Model = require('../../../models/wishlist-item-model');

Chai.should();

describe('## WISHLIST ITEM - MULTIPLE UPDATE RESOURCE ##', () => {
  const testTimeAt = new Date().toISOString();
  const itemId1 = 999999999;
  const itemId2 = 999999998;
  const newBody = [
    {
      id: itemId1, isPublic: true, productId: 2, name: `Test at ${testTimeAt}`,
    }, {
      id: itemId2, isPublic: true, productId: 2, name: `Test at ${testTimeAt}`,
    },
  ];
  const expiredAccessToken = '16J60MT18MqJdAnNabCImunX';
  const currentLib = {
    credentials: {
      authBaseUri: 'https://mc2m2z16rt5bqpz0jxhcp50yyg70.auth.marketingcloudapis.com',
      restBaseUri: 'https://mc2m2z16rt5bqpz0jxhcp50yyg70.rest.marketingcloudapis.com',
      tokenType: 'Bearer',
      accessToken: expiredAccessToken,
      clientId: 'eq7k9f4w0x6wf51z7f0vb3bd',
      clientSecret: 'qjKSY8GwM1C6iTreBNrNlmEc',
    },
    allowedObjects: [
      {
        assets: {
          dataExtensionName: 'osf_wishlist_item',
          max: 10,
          dataExtensionExternalKey: '7E8F8D24-8835-4ED3-A2B8-7FC50C19B863',
        },
      },
    ],
  };
  const obj = {
    infoOrganization: {
      accountId: 'admin@osf-global-dev.com',
    },
    privateLibraries: {
      MessageService: messageService,
      RedisService: redisService,
      Converters: {
        fieldsCore: {
          ...CoreItemStandard,
        },
        replaceFields: ConverterHelper.replaceFields,
      },
    },
    req: {
      params: {
        wishlistItemId: '',
      },
      body: {
        ...newBody,
      },
    },
    libObjId: 'wishlistItem',
  };

  it('#01 update items', (done) => {
    const updatedItems = [
      { id: itemId1, name: `Test at ${testTimeAt} UPDATED` },
      { id: itemId2, name: `Test at ${testTimeAt} UPDATED` },
    ];

    Model.createWishlistItems(obj, currentLib).then((successCreate) => {
      GenericHelper.isSuccess(successCreate.status).should.equal(true);

      obj.req.body = updatedItems;

      Model.updateWishlistItems(obj, currentLib).then((successUpdate) => {
        GenericHelper.isSuccess(successUpdate.status).should.equal(true);

        _.each(successUpdate.data, (item) => {
          const { id } = item;
          obj.req.params.wishlistItemId = id;

          Model.deleteWishlistItem(obj, currentLib).then((successDelete) => {
            GenericHelper.isSuccess(successDelete.status).should.equal(true);
          });
        });

        done();
      });
    });
  });

  // TO DO
  // fail on updating any non existing item
  // incorrect credentials
  // misspelled DE
  // incorrect DE external key
  // fail on updating with any item missing id
});
