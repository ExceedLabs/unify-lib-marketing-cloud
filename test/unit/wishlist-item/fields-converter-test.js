const Chai = require('chai');
const CoreItemStandard = require('../../resources/core/wishlist-item');
const ConverterService = require('../../../services/marketing/wishlist-item/wishlist-item-converter');
const ConverterHelper = require('../../resources/core/conversor-helper');
const GenericHelper = require('../../../services/helpers/unify-helpers/generic-helper');

Chai.should();

describe('## WISHLIST ITEM - FIELDS CONVERSION SERVICE ##', () => {
  it('#01 map wishlist item fields (app -> core)', () => {
    const testTimeAt = new Date().toISOString();
    const newBody = {
      id: 48, isPublic: true, productId: 2, name: `Item name. Test at ${testTimeAt}`,
    };

    const obj = {
      libObjId: 'wishlistItem',
      privateLibraries: {
        Converters: {
          fieldsCore: {
            ...CoreItemStandard,
          },
        },
      },
    };

    const result = ConverterService.getDataMapToCore(obj, [newBody]);

    result.map.list.should.equal('wishlistItem');
    result.map.item.id.should.equal('id');
    result.map.item.isPublic.should.equal('is_public');
    result.map.item.productId.should.equal('product_id');
    result.map.item.name.should.equal('name');
  });

  it('#02 replace wishlist item fields (app -> core)', () => {
    const testTimeAt = new Date().toISOString();
    const payload = {
      id: 48, isPublic: true, productId: 2, name: `Item name. Test at ${testTimeAt}`,
    };

    const obj = {
      libObjId: 'wishlistItem',
      privateLibraries: {
        Converters: {
          fieldsCore: {
            ...CoreItemStandard,
          },
          replaceFields: ConverterHelper.replaceFields,
        },
      },
    };

    const { map, data } = ConverterService.getDataMapToWishlistItem(obj, [payload]);
    const dataReplaced = obj.privateLibraries.Converters.replaceFields(data, map);
    const result = (dataReplaced.length) ? dataReplaced[0] : {};

    result.id.should.equal(48);
    result.is_public.should.equal(true);
    result.product_id.should.equal(2);
    result.name.should.equal(`Item name. Test at ${testTimeAt}`);
  });

  it('#03 remove null-valued fields from a replaced (app -> core) wishlist item payload', () => {
    const testTimeAt = new Date().toISOString();
    const payload = {
      id: 48, isPublic: true, productId: 2, name: `Item name. Test at ${testTimeAt}`,
    };

    const obj = {
      libObjId: 'wishlistItem',
      privateLibraries: {
        Converters: {
          fieldsCore: {
            ...CoreItemStandard,
          },
          replaceFields: ConverterHelper.replaceFields,
        },
      },
    };

    const { map, data } = ConverterService.getDataMapToWishlistItem(obj, [payload]);
    const dataReplaced = obj.privateLibraries.Converters.replaceFields(data, map);
    const result = (dataReplaced.length) ? dataReplaced[0] : {};
    const dataReplacedLength = Object.keys(result).length;
    const dataReplacedCleaned = GenericHelper.removeAttributesNull([result]);
    const dataReplacedCleanedLength = Object.keys(dataReplacedCleaned[0]).length;
    const condition = (dataReplacedCleanedLength < dataReplacedLength);

    condition.should.equal(true);
  });
});
