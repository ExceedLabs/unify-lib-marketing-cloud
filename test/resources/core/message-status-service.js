class MessageStatus {
  getSuccess(data = [], status = 200, message = 'success') {
    return {
      status,
      message,
      data,
    };
  }

  getError(data = [], status = 500, message = 'error') {
    return {
      status,
      message,
      data,
    };
  }
}

module.exports = new MessageStatus();
