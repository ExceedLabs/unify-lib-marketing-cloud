/**
 * This object represents a WISHLIST ITEM, which is an item desired by a customer.
 */
module.exports = {
  type: 'type',
  id: 'id',
  isPublic: 'isPublic',
  productId: 'productId',
  name: 'name',
  categoryId: 'categoryId',
  resourceState: 'resourceState',
  description: 'description',
  priority: 'priority',
  eventType: 'eventType',
  eventTypeValue: 'eventTypeValue',
  quantity: 'quantity',
  purchasedQuantity: 'purchasedQuantity',
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
};
