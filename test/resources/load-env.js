const dotenv = require('dotenv');

process.env.NODE_ENV = 'test';
dotenv.load({ path: `.env.${process.env.NODE_ENV}` });
