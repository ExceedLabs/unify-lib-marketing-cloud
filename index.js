const WishlistItemModel = require('./models/wishlist-item-model');

// WISHLIST ITEMS
exports.isAtLeastOneWishlistItemFromArrayInDataExtension = WishlistItemModel
  .isAtLeastOneWishlistItemFromArrayInDataExtension;
exports.isAtLeastOneWishlistItemFromArrayNotInDataExtension = WishlistItemModel
  .isAtLeastOneWishlistItemFromArrayNotInDataExtension;
exports.searchWishlistItemById = WishlistItemModel.searchWishlistItemById;
exports.listWishlistItems = WishlistItemModel.listWishlistItems;
exports.createWishlistItem = WishlistItemModel.createWishlistItem;
exports.createItem = WishlistItemModel.createItem;
exports.createWishlistItems = WishlistItemModel.createWishlistItems;
exports.createItems = WishlistItemModel.createItems;
exports.updateWishlistItem = WishlistItemModel.updateWishlistItem;
exports.updateItem = WishlistItemModel.updateItem;
exports.updateWishlistItems = WishlistItemModel.updateWishlistItems;
exports.updateItems = WishlistItemModel.updateItems;
exports.deleteWishlistItem = WishlistItemModel.deleteWishlistItem;
exports.formatUpsertResponse = WishlistItemModel.formatUpsertResponse;
exports.formatGetResponse = WishlistItemModel.formatGetResponse;
exports.formatDeleteResponse = WishlistItemModel.formatDeleteResponse;
