const ET_Client = require('fuelsdk-node');

class ETClient {
  static generateFuelSDKClient(clientId, clientSecret) {
    try {
      let IETClient;
      let stack;

      // avoid the UNABLE_TO_GET_ISSUER_CERT_LOCALLY soap error on s6.
      process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

      if (clientId) {
        IETClient = new ET_Client(clientId, clientSecret, stack);
      }

      return IETClient;
    } catch (error) {
      throw (error);
    }
  }
}

module.exports = ETClient;
